# Rahasak-Papers

We mainly involved with `distributed system`, `blockchain`, `federated learning`, 
`big data`, `machine-learning`, `cybersecurity`, `functional programming`,
`iot`, `edge computing`, `fog computing` etc research areas. This repository
contains the research papers that we(`@rahasak-labs`) have published in academic 
journals and conferences. our commercial products were built based on the concepts 
which we have published on these papers.
